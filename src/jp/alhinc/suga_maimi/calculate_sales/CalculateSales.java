package jp.alhinc.suga_maimi.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {
		System.out.println("ここにあるファイルを開きます =>" + args[0]);
		Map<String, String> map1 = new HashMap<String, String>();
		Map<String, Long> map2 = new HashMap<String, Long>();

		BufferedReader br = null;
		try {
			File file = new File(args[0], "branch.lst");
//エラー処理1-1
			if(!(file.exists())){
				System.out.println("支店定義ファイルが存在しません");
				return;
	        }
//支店定義保持
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
				while((line = br.readLine()) != null){
					String[] item = line.split(",");
					map1.put(item[0], item[1]);
					map2.put(item[0],0L);
//エラー処理1-2
					if(item.length != 2  || !item[0].matches("\\d{3}") || item[1].contains(".*\n.*")){
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}
				}
		}catch (IOException e){
		}finally{
			if(br != null){
				try{
				br.close();
				}catch (IOException e){
				}
			}
		}
//該当ファイル抽出
		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File file, String str) {
				if(str.matches("\\d{8}(.rcd)")) {
					return true;
				}else{
					return false;
				}
			}
		};
		BufferedReader br1 = null;

		try{
			File[] rcdfile = new File(args[0]).listFiles(filter);

			for(int y = 0 ; y < rcdfile.length ; y++){
				if(!rcdfile[y].isFile()){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
			for(int i = 0 ; i < rcdfile.length ; i++){
				File r = new File(rcdfile[i].toString());
				br1 = new BufferedReader(new FileReader(r));
	//エラー処理2-1
				String currentFile = rcdfile[0].getName();
				String currentFile2 = currentFile.substring(1, 8);
				int currentFile3=Integer.parseInt(currentFile2);

				for(int j = 0 ; j < rcdfile.length ; j++){

					String nextFile = rcdfile[j].getName();
					String nextFile2 = nextFile.substring(1, 8);
					int nextFile3=Integer.parseInt(nextFile2);

					if(currentFile3 != nextFile3){
						System.out.println("売上ファイル名が連番になっていません");
						return;
					}
					currentFile3++;
				}

					String line1 = br1.readLine();
					String line2 = br1.readLine();
	//エラー処理2-3
				if(!map1.containsKey(line1)){
					System.out.println(rcdfile[i].getName()+"の支店コードが不正です");
					return;
				}
	//エラー処理2-4
				if(br1.read() > 3){
					System.out.println(rcdfile[i].getName()+"のフォーマットが不正です");
					return;
				}
	//売上集計
				Long rcdSales = Long.parseLong(line2);
				Long sum = rcdSales + map2.get(line1);
				map2.put(line1,sum);
	//エラー処理2-2
				String stringSum = String.valueOf(sum);
				if(stringSum.length() >= 11){
					System.out.println("合計金額が10桁を超えました");
					return;
				}
			}
		}catch (IOException e){
		}finally{
			if(br1 != null){
				try{
				br1.close();
				}catch (IOException e){
				}
			}
		}
//集計結果出力
		BufferedWriter bw = null;
		try{
			File file = new File(args[0],"branch.out");
			FileWriter fr = new FileWriter(file);
			bw = new BufferedWriter(fr);

			for(String key : map1.keySet()){
				bw.write(key + "," + map1.get(key)+ "," + map2.get(key));
				bw.newLine();
			}
		}catch (IOException e){
				System.out.println("予期せぬエラーが発生しました");
		}finally{
			try{
			bw.close();
			}catch (IOException e){
			}
		}
	}
}
